package de.stoitschev.recipeapp.domain.enumeration;

/**
 * The Difficulty enumeration.
 */
public enum Difficulty {
    EASY, MODERATE, HARD
}
