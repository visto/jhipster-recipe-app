/**
 * View Models used by Spring MVC REST controllers.
 */
package de.stoitschev.recipeapp.web.rest.vm;
