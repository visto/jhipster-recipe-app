import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { INotes, defaultValue } from 'app/shared/model/notes.model';

export const ACTION_TYPES = {
  FETCH_NOTES_LIST: 'notes/FETCH_NOTES_LIST',
  FETCH_NOTES: 'notes/FETCH_NOTES',
  CREATE_NOTES: 'notes/CREATE_NOTES',
  UPDATE_NOTES: 'notes/UPDATE_NOTES',
  DELETE_NOTES: 'notes/DELETE_NOTES',
  RESET: 'notes/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<INotes>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false
};

export type NotesState = Readonly<typeof initialState>;

// Reducer

export default (state: NotesState = initialState, action): NotesState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_NOTES_LIST):
    case REQUEST(ACTION_TYPES.FETCH_NOTES):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_NOTES):
    case REQUEST(ACTION_TYPES.UPDATE_NOTES):
    case REQUEST(ACTION_TYPES.DELETE_NOTES):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_NOTES_LIST):
    case FAILURE(ACTION_TYPES.FETCH_NOTES):
    case FAILURE(ACTION_TYPES.CREATE_NOTES):
    case FAILURE(ACTION_TYPES.UPDATE_NOTES):
    case FAILURE(ACTION_TYPES.DELETE_NOTES):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_NOTES_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_NOTES):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_NOTES):
    case SUCCESS(ACTION_TYPES.UPDATE_NOTES):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_NOTES):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/notes';

// Actions

export const getEntities: ICrudGetAllAction<INotes> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_NOTES_LIST,
  payload: axios.get<INotes>(`${apiUrl}?cacheBuster=${new Date().getTime()}`)
});

export const getEntity: ICrudGetAction<INotes> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_NOTES,
    payload: axios.get<INotes>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<INotes> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_NOTES,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<INotes> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_NOTES,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<INotes> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_NOTES,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
