import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAllAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './unit-of-measure.reducer';
import { IUnitOfMeasure } from 'app/shared/model/unit-of-measure.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IUnitOfMeasureProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export class UnitOfMeasure extends React.Component<IUnitOfMeasureProps> {
  componentDidMount() {
    this.props.getEntities();
  }

  render() {
    const { unitOfMeasureList, match } = this.props;
    return (
      <div>
        <h2 id="unit-of-measure-heading">
          <Translate contentKey="recipeappApp.unitOfMeasure.home.title">Unit Of Measures</Translate>
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />
            &nbsp;
            <Translate contentKey="recipeappApp.unitOfMeasure.home.createLabel">Create new Unit Of Measure</Translate>
          </Link>
        </h2>
        <div className="table-responsive">
          <Table responsive>
            <thead>
              <tr>
                <th>
                  <Translate contentKey="global.field.id">ID</Translate>
                </th>
                <th>
                  <Translate contentKey="recipeappApp.unitOfMeasure.uom">Uom</Translate>
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {unitOfMeasureList.map((unitOfMeasure, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${unitOfMeasure.id}`} color="link" size="sm">
                      {unitOfMeasure.id}
                    </Button>
                  </td>
                  <td>{unitOfMeasure.uom}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${unitOfMeasure.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${unitOfMeasure.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${unitOfMeasure.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ unitOfMeasure }: IRootState) => ({
  unitOfMeasureList: unitOfMeasure.entities
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UnitOfMeasure);
