import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import UnitOfMeasure from './unit-of-measure';
import UnitOfMeasureDetail from './unit-of-measure-detail';
import UnitOfMeasureUpdate from './unit-of-measure-update';
import UnitOfMeasureDeleteDialog from './unit-of-measure-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={UnitOfMeasureUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={UnitOfMeasureUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={UnitOfMeasureDetail} />
      <ErrorBoundaryRoute path={match.url} component={UnitOfMeasure} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={UnitOfMeasureDeleteDialog} />
  </>
);

export default Routes;
