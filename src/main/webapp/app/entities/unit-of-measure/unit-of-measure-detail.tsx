import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './unit-of-measure.reducer';
import { IUnitOfMeasure } from 'app/shared/model/unit-of-measure.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IUnitOfMeasureDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class UnitOfMeasureDetail extends React.Component<IUnitOfMeasureDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { unitOfMeasureEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="recipeappApp.unitOfMeasure.detail.title">UnitOfMeasure</Translate> [<b>{unitOfMeasureEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="uom">
                <Translate contentKey="recipeappApp.unitOfMeasure.uom">Uom</Translate>
              </span>
            </dt>
            <dd>{unitOfMeasureEntity.uom}</dd>
          </dl>
          <Button tag={Link} to="/entity/unit-of-measure" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/unit-of-measure/${unitOfMeasureEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ unitOfMeasure }: IRootState) => ({
  unitOfMeasureEntity: unitOfMeasure.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UnitOfMeasureDetail);
