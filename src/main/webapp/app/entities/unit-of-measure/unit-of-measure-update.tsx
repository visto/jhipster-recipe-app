import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IIngredient } from 'app/shared/model/ingredient.model';
import { getEntities as getIngredients } from 'app/entities/ingredient/ingredient.reducer';
import { getEntity, updateEntity, createEntity, reset } from './unit-of-measure.reducer';
import { IUnitOfMeasure } from 'app/shared/model/unit-of-measure.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IUnitOfMeasureUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IUnitOfMeasureUpdateState {
  isNew: boolean;
  ingredientId: string;
}

export class UnitOfMeasureUpdate extends React.Component<IUnitOfMeasureUpdateProps, IUnitOfMeasureUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      ingredientId: '0',
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getIngredients();
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { unitOfMeasureEntity } = this.props;
      const entity = {
        ...unitOfMeasureEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/unit-of-measure');
  };

  render() {
    const { unitOfMeasureEntity, ingredients, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="recipeappApp.unitOfMeasure.home.createOrEditLabel">
              <Translate contentKey="recipeappApp.unitOfMeasure.home.createOrEditLabel">Create or edit a UnitOfMeasure</Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : unitOfMeasureEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="unit-of-measure-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="uomLabel" for="uom">
                    <Translate contentKey="recipeappApp.unitOfMeasure.uom">Uom</Translate>
                  </Label>
                  <AvField id="unit-of-measure-uom" type="text" name="uom" />
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/unit-of-measure" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  ingredients: storeState.ingredient.entities,
  unitOfMeasureEntity: storeState.unitOfMeasure.entity,
  loading: storeState.unitOfMeasure.loading,
  updating: storeState.unitOfMeasure.updating,
  updateSuccess: storeState.unitOfMeasure.updateSuccess
});

const mapDispatchToProps = {
  getIngredients,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UnitOfMeasureUpdate);
