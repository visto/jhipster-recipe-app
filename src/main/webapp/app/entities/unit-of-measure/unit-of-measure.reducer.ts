import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IUnitOfMeasure, defaultValue } from 'app/shared/model/unit-of-measure.model';

export const ACTION_TYPES = {
  FETCH_UNITOFMEASURE_LIST: 'unitOfMeasure/FETCH_UNITOFMEASURE_LIST',
  FETCH_UNITOFMEASURE: 'unitOfMeasure/FETCH_UNITOFMEASURE',
  CREATE_UNITOFMEASURE: 'unitOfMeasure/CREATE_UNITOFMEASURE',
  UPDATE_UNITOFMEASURE: 'unitOfMeasure/UPDATE_UNITOFMEASURE',
  DELETE_UNITOFMEASURE: 'unitOfMeasure/DELETE_UNITOFMEASURE',
  RESET: 'unitOfMeasure/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IUnitOfMeasure>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false
};

export type UnitOfMeasureState = Readonly<typeof initialState>;

// Reducer

export default (state: UnitOfMeasureState = initialState, action): UnitOfMeasureState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_UNITOFMEASURE_LIST):
    case REQUEST(ACTION_TYPES.FETCH_UNITOFMEASURE):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_UNITOFMEASURE):
    case REQUEST(ACTION_TYPES.UPDATE_UNITOFMEASURE):
    case REQUEST(ACTION_TYPES.DELETE_UNITOFMEASURE):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_UNITOFMEASURE_LIST):
    case FAILURE(ACTION_TYPES.FETCH_UNITOFMEASURE):
    case FAILURE(ACTION_TYPES.CREATE_UNITOFMEASURE):
    case FAILURE(ACTION_TYPES.UPDATE_UNITOFMEASURE):
    case FAILURE(ACTION_TYPES.DELETE_UNITOFMEASURE):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_UNITOFMEASURE_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_UNITOFMEASURE):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_UNITOFMEASURE):
    case SUCCESS(ACTION_TYPES.UPDATE_UNITOFMEASURE):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_UNITOFMEASURE):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/unit-of-measures';

// Actions

export const getEntities: ICrudGetAllAction<IUnitOfMeasure> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_UNITOFMEASURE_LIST,
  payload: axios.get<IUnitOfMeasure>(`${apiUrl}?cacheBuster=${new Date().getTime()}`)
});

export const getEntity: ICrudGetAction<IUnitOfMeasure> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_UNITOFMEASURE,
    payload: axios.get<IUnitOfMeasure>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IUnitOfMeasure> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_UNITOFMEASURE,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IUnitOfMeasure> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_UNITOFMEASURE,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IUnitOfMeasure> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_UNITOFMEASURE,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
