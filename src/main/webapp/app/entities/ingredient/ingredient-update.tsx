import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IUnitOfMeasure } from 'app/shared/model/unit-of-measure.model';
import { getEntities as getUnitOfMeasures } from 'app/entities/unit-of-measure/unit-of-measure.reducer';
import { IRecipe } from 'app/shared/model/recipe.model';
import { getEntities as getRecipes } from 'app/entities/recipe/recipe.reducer';
import { getEntity, updateEntity, createEntity, reset } from './ingredient.reducer';
import { IIngredient } from 'app/shared/model/ingredient.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IIngredientUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IIngredientUpdateState {
  isNew: boolean;
  unitOfMeasureId: string;
  recipeId: string;
}

export class IngredientUpdate extends React.Component<IIngredientUpdateProps, IIngredientUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      unitOfMeasureId: '0',
      recipeId: '0',
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getUnitOfMeasures();
    this.props.getRecipes();
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { ingredientEntity } = this.props;
      const entity = {
        ...ingredientEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/ingredient');
  };

  render() {
    const { ingredientEntity, unitOfMeasures, recipes, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="recipeappApp.ingredient.home.createOrEditLabel">
              <Translate contentKey="recipeappApp.ingredient.home.createOrEditLabel">Create or edit a Ingredient</Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : ingredientEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="ingredient-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="descriptionLabel" for="description">
                    <Translate contentKey="recipeappApp.ingredient.description">Description</Translate>
                  </Label>
                  <AvField id="ingredient-description" type="text" name="description" />
                </AvGroup>
                <AvGroup>
                  <Label id="amountLabel" for="amount">
                    <Translate contentKey="recipeappApp.ingredient.amount">Amount</Translate>
                  </Label>
                  <AvField id="ingredient-amount" type="text" name="amount" />
                </AvGroup>
                <AvGroup>
                  <Label for="unitOfMeasure.id">
                    <Translate contentKey="recipeappApp.ingredient.unitOfMeasure">Unit Of Measure</Translate>
                  </Label>
                  <AvInput id="ingredient-unitOfMeasure" type="select" className="form-control" name="unitOfMeasure.id">
                    <option value="" key="0" />
                    {unitOfMeasures
                      ? unitOfMeasures.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.id}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <AvGroup>
                  <Label for="recipe.id">
                    <Translate contentKey="recipeappApp.ingredient.recipe">Recipe</Translate>
                  </Label>
                  <AvInput id="ingredient-recipe" type="select" className="form-control" name="recipe.id">
                    <option value="" key="0" />
                    {recipes
                      ? recipes.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.id}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/ingredient" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  unitOfMeasures: storeState.unitOfMeasure.entities,
  recipes: storeState.recipe.entities,
  ingredientEntity: storeState.ingredient.entity,
  loading: storeState.ingredient.loading,
  updating: storeState.ingredient.updating,
  updateSuccess: storeState.ingredient.updateSuccess
});

const mapDispatchToProps = {
  getUnitOfMeasures,
  getRecipes,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(IngredientUpdate);
