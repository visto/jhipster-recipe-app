import React from 'react';
import { Switch } from 'react-router-dom';

// tslint:disable-next-line:no-unused-variable
import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Recipe from './recipe';
import Notes from './notes';
import Ingredient from './ingredient';
import Category from './category';
import UnitOfMeasure from './unit-of-measure';
/* jhipster-needle-add-route-import - JHipster will add routes here */

const Routes = ({ match }) => (
  <div>
    <Switch>
      {/* prettier-ignore */}
      <ErrorBoundaryRoute path={`${match.url}/recipe`} component={Recipe} />
      <ErrorBoundaryRoute path={`${match.url}/notes`} component={Notes} />
      <ErrorBoundaryRoute path={`${match.url}/ingredient`} component={Ingredient} />
      <ErrorBoundaryRoute path={`${match.url}/category`} component={Category} />
      <ErrorBoundaryRoute path={`${match.url}/unit-of-measure`} component={UnitOfMeasure} />
      {/* jhipster-needle-add-route-path - JHipster will routes here */}
    </Switch>
  </div>
);

export default Routes;
