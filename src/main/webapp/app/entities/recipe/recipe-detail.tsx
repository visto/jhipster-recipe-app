import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction, openFile, byteSize } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './recipe.reducer';
import { IRecipe } from 'app/shared/model/recipe.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IRecipeDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class RecipeDetail extends React.Component<IRecipeDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { recipeEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="recipeappApp.recipe.detail.title">Recipe</Translate> [<b>{recipeEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="description">
                <Translate contentKey="recipeappApp.recipe.description">Description</Translate>
              </span>
            </dt>
            <dd>{recipeEntity.description}</dd>
            <dt>
              <span id="difficulty">
                <Translate contentKey="recipeappApp.recipe.difficulty">Difficulty</Translate>
              </span>
            </dt>
            <dd>{recipeEntity.difficulty}</dd>
            <dt>
              <span id="preptime">
                <Translate contentKey="recipeappApp.recipe.preptime">Preptime</Translate>
              </span>
            </dt>
            <dd>{recipeEntity.preptime}</dd>
            <dt>
              <span id="cookTime">
                <Translate contentKey="recipeappApp.recipe.cookTime">Cook Time</Translate>
              </span>
            </dt>
            <dd>{recipeEntity.cookTime}</dd>
            <dt>
              <span id="servings">
                <Translate contentKey="recipeappApp.recipe.servings">Servings</Translate>
              </span>
            </dt>
            <dd>{recipeEntity.servings}</dd>
            <dt>
              <span id="source">
                <Translate contentKey="recipeappApp.recipe.source">Source</Translate>
              </span>
            </dt>
            <dd>{recipeEntity.source}</dd>
            <dt>
              <span id="url">
                <Translate contentKey="recipeappApp.recipe.url">Url</Translate>
              </span>
            </dt>
            <dd>{recipeEntity.url}</dd>
            <dt>
              <span id="directions">
                <Translate contentKey="recipeappApp.recipe.directions">Directions</Translate>
              </span>
            </dt>
            <dd>{recipeEntity.directions}</dd>
            <dt>
              <span id="image">
                <Translate contentKey="recipeappApp.recipe.image">Image</Translate>
              </span>
            </dt>
            <dd>
              {recipeEntity.image ? (
                <div>
                  <a onClick={openFile(recipeEntity.imageContentType, recipeEntity.image)}>
                    <img src={`data:${recipeEntity.imageContentType};base64,${recipeEntity.image}`} style={{ maxHeight: '30px' }} />
                  </a>
                  <span>
                    {recipeEntity.imageContentType}, {byteSize(recipeEntity.image)}
                  </span>
                </div>
              ) : null}
            </dd>
            <dt>
              <Translate contentKey="recipeappApp.recipe.notes">Notes</Translate>
            </dt>
            <dd>{recipeEntity.notes ? recipeEntity.notes.id : ''}</dd>
            <dt>
              <Translate contentKey="recipeappApp.recipe.category">Category</Translate>
            </dt>
            <dd>
              {recipeEntity.categories
                ? recipeEntity.categories.map((val, i) => (
                    <span key={val.id}>
                      <a>{val.id}</a>
                      {i === recipeEntity.categories.length - 1 ? '' : ', '}
                    </span>
                  ))
                : null}
            </dd>
          </dl>
          <Button tag={Link} to="/entity/recipe" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/recipe/${recipeEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ recipe }: IRootState) => ({
  recipeEntity: recipe.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RecipeDetail);
