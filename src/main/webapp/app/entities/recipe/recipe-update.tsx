import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, setFileData, openFile, byteSize, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { INotes } from 'app/shared/model/notes.model';
import { getEntities as getNotes } from 'app/entities/notes/notes.reducer';
import { ICategory } from 'app/shared/model/category.model';
import { getEntities as getCategories } from 'app/entities/category/category.reducer';
import { getEntity, updateEntity, createEntity, setBlob, reset } from './recipe.reducer';
import { IRecipe } from 'app/shared/model/recipe.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IRecipeUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IRecipeUpdateState {
  isNew: boolean;
  idscategory: any[];
  notesId: string;
}

export class RecipeUpdate extends React.Component<IRecipeUpdateProps, IRecipeUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      idscategory: [],
      notesId: '0',
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getNotes();
    this.props.getCategories();
  }

  onBlobChange = (isAnImage, name) => event => {
    setFileData(event, (contentType, data) => this.props.setBlob(name, data, contentType), isAnImage);
  };

  clearBlob = name => () => {
    this.props.setBlob(name, undefined, undefined);
  };

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { recipeEntity } = this.props;
      const entity = {
        ...recipeEntity,
        ...values,
        categories: mapIdList(values.categories)
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/recipe');
  };

  render() {
    const { recipeEntity, notes, categories, loading, updating } = this.props;
    const { isNew } = this.state;

    const { image, imageContentType } = recipeEntity;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="recipeappApp.recipe.home.createOrEditLabel">
              <Translate contentKey="recipeappApp.recipe.home.createOrEditLabel">Create or edit a Recipe</Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : recipeEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="recipe-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="descriptionLabel" for="description">
                    <Translate contentKey="recipeappApp.recipe.description">Description</Translate>
                  </Label>
                  <AvField id="recipe-description" type="text" name="description" />
                </AvGroup>
                <AvGroup>
                  <Label id="difficultyLabel">
                    <Translate contentKey="recipeappApp.recipe.difficulty">Difficulty</Translate>
                  </Label>
                  <AvInput
                    id="recipe-difficulty"
                    type="select"
                    className="form-control"
                    name="difficulty"
                    value={(!isNew && recipeEntity.difficulty) || 'EASY'}
                  >
                    <option value="EASY">
                      <Translate contentKey="recipeappApp.Difficulty.EASY" />
                    </option>
                    <option value="MODERATE">
                      <Translate contentKey="recipeappApp.Difficulty.MODERATE" />
                    </option>
                    <option value="HARD">
                      <Translate contentKey="recipeappApp.Difficulty.HARD" />
                    </option>
                  </AvInput>
                </AvGroup>
                <AvGroup>
                  <Label id="preptimeLabel" for="preptime">
                    <Translate contentKey="recipeappApp.recipe.preptime">Preptime</Translate>
                  </Label>
                  <AvField id="recipe-preptime" type="string" className="form-control" name="preptime" />
                </AvGroup>
                <AvGroup>
                  <Label id="cookTimeLabel" for="cookTime">
                    <Translate contentKey="recipeappApp.recipe.cookTime">Cook Time</Translate>
                  </Label>
                  <AvField id="recipe-cookTime" type="string" className="form-control" name="cookTime" />
                </AvGroup>
                <AvGroup>
                  <Label id="servingsLabel" for="servings">
                    <Translate contentKey="recipeappApp.recipe.servings">Servings</Translate>
                  </Label>
                  <AvField id="recipe-servings" type="string" className="form-control" name="servings" />
                </AvGroup>
                <AvGroup>
                  <Label id="sourceLabel" for="source">
                    <Translate contentKey="recipeappApp.recipe.source">Source</Translate>
                  </Label>
                  <AvField id="recipe-source" type="text" name="source" />
                </AvGroup>
                <AvGroup>
                  <Label id="urlLabel" for="url">
                    <Translate contentKey="recipeappApp.recipe.url">Url</Translate>
                  </Label>
                  <AvField id="recipe-url" type="text" name="url" />
                </AvGroup>
                <AvGroup>
                  <Label id="directionsLabel" for="directions">
                    <Translate contentKey="recipeappApp.recipe.directions">Directions</Translate>
                  </Label>
                  <AvField id="recipe-directions" type="text" name="directions" />
                </AvGroup>
                <AvGroup>
                  <AvGroup>
                    <Label id="imageLabel" for="image">
                      <Translate contentKey="recipeappApp.recipe.image">Image</Translate>
                    </Label>
                    <br />
                    {image ? (
                      <div>
                        <a onClick={openFile(imageContentType, image)}>
                          <img src={`data:${imageContentType};base64,${image}`} style={{ maxHeight: '100px' }} />
                        </a>
                        <br />
                        <Row>
                          <Col md="11">
                            <span>
                              {imageContentType}, {byteSize(image)}
                            </span>
                          </Col>
                          <Col md="1">
                            <Button color="danger" onClick={this.clearBlob('image')}>
                              <FontAwesomeIcon icon="times-circle" />
                            </Button>
                          </Col>
                        </Row>
                      </div>
                    ) : null}
                    <input id="file_image" type="file" onChange={this.onBlobChange(true, 'image')} accept="image/*" />
                    <AvInput type="hidden" name="image" value={image} />
                  </AvGroup>
                </AvGroup>
                <AvGroup>
                  <Label for="notes.id">
                    <Translate contentKey="recipeappApp.recipe.notes">Notes</Translate>
                  </Label>
                  <AvInput id="recipe-notes" type="select" className="form-control" name="notes.id">
                    <option value="" key="0" />
                    {notes
                      ? notes.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.id}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <AvGroup>
                  <Label for="categories">
                    <Translate contentKey="recipeappApp.recipe.category">Category</Translate>
                  </Label>
                  <AvInput
                    id="recipe-category"
                    type="select"
                    multiple
                    className="form-control"
                    name="categories"
                    value={recipeEntity.categories && recipeEntity.categories.map(e => e.id)}
                  >
                    <option value="" key="0" />
                    {categories
                      ? categories.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.id}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/recipe" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  notes: storeState.notes.entities,
  categories: storeState.category.entities,
  recipeEntity: storeState.recipe.entity,
  loading: storeState.recipe.loading,
  updating: storeState.recipe.updating,
  updateSuccess: storeState.recipe.updateSuccess
});

const mapDispatchToProps = {
  getNotes,
  getCategories,
  getEntity,
  updateEntity,
  setBlob,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RecipeUpdate);
