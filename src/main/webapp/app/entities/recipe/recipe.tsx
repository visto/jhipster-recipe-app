import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { openFile, byteSize, Translate, ICrudGetAllAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './recipe.reducer';
import { IRecipe } from 'app/shared/model/recipe.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IRecipeProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export class Recipe extends React.Component<IRecipeProps> {
  componentDidMount() {
    this.props.getEntities();
  }

  render() {
    const { recipeList, match } = this.props;
    return (
      <div>
        <h2 id="recipe-heading">
          <Translate contentKey="recipeappApp.recipe.home.title">Recipes</Translate>
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />
            &nbsp;
            <Translate contentKey="recipeappApp.recipe.home.createLabel">Create new Recipe</Translate>
          </Link>
        </h2>
        <div className="table-responsive">
          <Table responsive>
            <thead>
              <tr>
                <th>
                  <Translate contentKey="global.field.id">ID</Translate>
                </th>
                <th>
                  <Translate contentKey="recipeappApp.recipe.description">Description</Translate>
                </th>
                <th>
                  <Translate contentKey="recipeappApp.recipe.difficulty">Difficulty</Translate>
                </th>
                <th>
                  <Translate contentKey="recipeappApp.recipe.preptime">Preptime</Translate>
                </th>
                <th>
                  <Translate contentKey="recipeappApp.recipe.cookTime">Cook Time</Translate>
                </th>
                <th>
                  <Translate contentKey="recipeappApp.recipe.servings">Servings</Translate>
                </th>
                <th>
                  <Translate contentKey="recipeappApp.recipe.source">Source</Translate>
                </th>
                <th>
                  <Translate contentKey="recipeappApp.recipe.url">Url</Translate>
                </th>
                <th>
                  <Translate contentKey="recipeappApp.recipe.directions">Directions</Translate>
                </th>
                <th>
                  <Translate contentKey="recipeappApp.recipe.image">Image</Translate>
                </th>
                <th>
                  <Translate contentKey="recipeappApp.recipe.notes">Notes</Translate>
                </th>
                <th>
                  <Translate contentKey="recipeappApp.recipe.category">Category</Translate>
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {recipeList.map((recipe, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${recipe.id}`} color="link" size="sm">
                      {recipe.id}
                    </Button>
                  </td>
                  <td>{recipe.description}</td>
                  <td>
                    <Translate contentKey={`recipeappApp.Difficulty.${recipe.difficulty}`} />
                  </td>
                  <td>{recipe.preptime}</td>
                  <td>{recipe.cookTime}</td>
                  <td>{recipe.servings}</td>
                  <td>{recipe.source}</td>
                  <td>{recipe.url}</td>
                  <td>{recipe.directions}</td>
                  <td>
                    {recipe.image ? (
                      <div>
                        <a onClick={openFile(recipe.imageContentType, recipe.image)}>
                          <img src={`data:${recipe.imageContentType};base64,${recipe.image}`} style={{ maxHeight: '30px' }} />
                          &nbsp;
                        </a>
                        <span>
                          {recipe.imageContentType}, {byteSize(recipe.image)}
                        </span>
                      </div>
                    ) : null}
                  </td>
                  <td>{recipe.notes ? <Link to={`notes/${recipe.notes.id}`}>{recipe.notes.id}</Link> : ''}</td>
                  <td>
                    {recipe.categories
                      ? recipe.categories.map((val, j) => (
                          <span key={j}>
                            <Link to={`category/${val.id}`}>{val.id}</Link>
                            {j === recipe.categories.length - 1 ? '' : ', '}
                          </span>
                        ))
                      : null}
                  </td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${recipe.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${recipe.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${recipe.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ recipe }: IRootState) => ({
  recipeList: recipe.entities
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Recipe);
