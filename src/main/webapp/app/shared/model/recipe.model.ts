import { INotes } from 'app/shared/model//notes.model';
import { IIngredient } from 'app/shared/model//ingredient.model';
import { ICategory } from 'app/shared/model//category.model';

export const enum Difficulty {
  EASY = 'EASY',
  MODERATE = 'MODERATE',
  HARD = 'HARD'
}

export interface IRecipe {
  id?: number;
  description?: string;
  difficulty?: Difficulty;
  preptime?: number;
  cookTime?: number;
  servings?: number;
  source?: string;
  url?: string;
  directions?: string;
  imageContentType?: string;
  image?: any;
  notes?: INotes;
  ingredients?: IIngredient[];
  categories?: ICategory[];
}

export const defaultValue: Readonly<IRecipe> = {};
