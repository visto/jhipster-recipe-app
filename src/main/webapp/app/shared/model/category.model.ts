import { IRecipe } from 'app/shared/model//recipe.model';

export interface ICategory {
  id?: number;
  categoryName?: string;
  recipes?: IRecipe[];
}

export const defaultValue: Readonly<ICategory> = {};
