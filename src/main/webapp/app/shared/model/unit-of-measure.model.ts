import { IIngredient } from 'app/shared/model//ingredient.model';

export interface IUnitOfMeasure {
  id?: number;
  uom?: string;
  ingredient?: IIngredient;
}

export const defaultValue: Readonly<IUnitOfMeasure> = {};
