import { IUnitOfMeasure } from 'app/shared/model//unit-of-measure.model';
import { IRecipe } from 'app/shared/model//recipe.model';

export interface IIngredient {
  id?: number;
  description?: string;
  amount?: number;
  unitOfMeasure?: IUnitOfMeasure;
  recipe?: IRecipe;
}

export const defaultValue: Readonly<IIngredient> = {};
