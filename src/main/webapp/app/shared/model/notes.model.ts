import { IRecipe } from 'app/shared/model//recipe.model';

export interface INotes {
  id?: number;
  notes?: string;
  recipe?: IRecipe;
}

export const defaultValue: Readonly<INotes> = {};
